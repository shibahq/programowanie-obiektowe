from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields.simple import PasswordField

from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Length


class AddForm(FlaskForm):
    username = StringField('Pracownik', validators=[DataRequired(), Length(min=1, max=128)])
    password = PasswordField('Hasło', validators=[DataRequired()])
    submit = SubmitField('Dodaj pracownika')


class LoginForm(FlaskForm):
    username = StringField('Pracownik', validators=[DataRequired()])
    password = PasswordField('Hasło', validators=[DataRequired()])
    submit = SubmitField('Zaloguj')
    