from flask_wtf import FlaskForm

from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired


class MenuForm(FlaskForm):
    day = StringField('Dzień', validators=[DataRequired()])
    menu = TextAreaField('Menu', validators=[DataRequired()])
    submit = SubmitField('Generuj')