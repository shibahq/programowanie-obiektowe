import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from menu.config import Config

db = SQLAlchemy()
login_manager = LoginManager()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)


    db.init_app(app)

    with app.app_context():
        db.create_all()

    login_manager.init_app(app)
    

    from menu.controllers import user
    app.register_blueprint(user.app)

    from menu.controllers import menu
    app.register_blueprint(menu.app)
    app.add_url_rule('/', endpoint='index')

    return app