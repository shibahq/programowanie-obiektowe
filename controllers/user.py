import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask_login import login_user, current_user, logout_user, login_required
from menu import db
from menu.models import User
from menu.forms.user import (AddForm, LoginForm)
from werkzeug.security import check_password_hash, generate_password_hash


app = Blueprint('user', __name__, url_prefix=('/user'))

@app.route('/add', methods=('GET', 'POST'))
def register():
    form = AddForm()
    if form.validate_on_submit():
        pass_hash = generate_password_hash(form.password.data)
        user = User(username=form.username.data, password=pass_hash)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('user.login'))
    return render_template('user/add.html', title='Dodaj pracownika', form=form)

@app.route('/login', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('index'))
        else:
            flash('Logowanie nie powiodło się')
    return render_template('user/login.html', title='Zaloguj się', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

