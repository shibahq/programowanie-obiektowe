import pdfkit
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, make_response
)
from flask_login import current_user, login_required
from menu import db
from menu.models import Menu
from menu.forms.menu import MenuForm
from werkzeug.exceptions import abort


app = Blueprint('menu', __name__)


@app.route('/')
def index():
    menu = Menu.query.get_or_404(id)
    return render_template('menu/index.html', menus=menus)


@app.route('/add', methods=('GET', 'POST'))
@login_required
def add():
    form = MenuForm()
    if form.validate_on_submit():
        menu = Menu(day=form.day.data, menu=form.menu.data, author=current_user)
        db.session.add(menu)
        db.session.commit()
        flash('Dodano kartę menu')
        return redirect(url_for('menu.index'))

    return render_template('menu/add.html', title='Dodaj kartę menu', form=form)

@app.route('/generate/<int:id>', methods=('GET',))
def generate_pdf(id):
    menu = Menu.query.get_or_404(id)
    pdf = render_template('pdf/pdf.html', menu=menu)
    generate = pdfkit.from_string(pdf)

    run = make_response(generate)
    run.headers['Content-Type'] = 'application/pdf'
    run.headers['Content-Disposition'] = 'inline; filename=karta.pdf'
    return run

@app.route('/<int:id>')
def view(id):
    menu = Menu.query.get_or_404(id)
    return render_template('menu/view.html', menu=menu)


@app.route('/<int:id>/update', methods=('GET', 'POST'))
def update(id):
    menu = Menu.query.get_or_404(id)
    if form.validate_on_submit():
        menu.day = form.day.data
        menu.menu = form.menu.data
        db.session.commit()
        return redirect(url_for('menu.menu', id=id))
    
    return render_template('menu/update.html', menu=menu)


@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    menu = Menu.query.get_or_404(id)
    db.session.delete(menu)
    db.session.commit()
    return redirect(url_for('menu.index'))