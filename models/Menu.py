from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from menu import db, login_manager

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Menu(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    day = db.Column(db.String(120), nullable=False)
    menu = db.Column(db.String(900000), nullable=False)
    created = db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)    

    def __repr__(self):
        return f"Post('{self.day}', '{self.menu}')"